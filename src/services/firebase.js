import firebase from 'firebase'

var config = {
  apiKey: 'AIzaSyAvaEwPYUVaqAAB1AFGLJcWny2ujMKkSF0',
  authDomain: 'cropchat-c193c.firebaseapp.com',
  databaseURL: 'https://cropchat-c193c.firebaseio.com',
  projectId: 'cropchat-c193c',
  storageBucket: 'cropchat-c193c.appspot.com',
  messagingSenderId: '557871743309'
}

firebase.initializeApp(config)

const database = firebase.database()
const storage = firebase.storage()

export {
  database,
  storage
}
